# Project 4: Brevet time calculator with Ajax

Reimplementation of the RUSA ACP controle time calculator with flask and ajax.

Created by Miles Wright for CIS 322 at the University of Oregon

#Use

To run the server, change directory to /brevet

Run the command docker build -t brevet

Then run the command docker run -d -p 5000:5000 brevet

After the above commands are run, open a web browser and go to localhost:5000

Here you should be met with a page where you can change the date to whatever starting date you wish then you can
input distances for checkpoints or control points and after inputting each distance the date to open the point and 
close the point will be displayed. 

#In case of future rule changes

You can replace the lists acp_times.py to represent any future rule changes. 

#The calculations

There is a time matching up to each distance for example 0-200km has a max pase of 34km/hr and a minimum pase of 15km/hr
then for 200-400km the max pase is 32km/hr and a minimum pase of 15km/hr therefore to calculate the open time (fastest pase)
So if we wanted a control point at 350km it would need to be open from 200 / 34 + 150 / 23 hours after the start of the race 
until 200 / 15 + 150 / 15 hours after the start of the race so therefore we can find the open and close times based on the start time.
