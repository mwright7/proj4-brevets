"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # if the control_dist is less then an entry in the dict then the required time is the dist times the rate in dict. 
    list_speed = [(0, 0), (200, 34), (400, 32), (600, 30), (1000, 28), (1300, 26)]
    more_time = 0
    for i in range(1, len(list_speed)):
        if(control_dist_km > list_speed[i][0]):
            # km / km/hr = hr --- hr * 60 = min
            more_time += (60 * (list_speed[i][0] - list_speed[i-1][0])) / list_speed[i][1]
        elif(control_dist_km <= list_speed[i][0]):
            if i == 0:
                pass
            else: #i != 0
                distance_remaining = control_dist_km - list_speed[i-1][0]
                more_time += (60 * distance_remaining) / list_speed[i][1]
                break

    # more time is in minutes right now
    more_time = round(more_time)
    hours_shift = more_time // 60
    minutes_shift = more_time % 60
    open_time = brevet_start_time.clone()
    open_time = open_time.shift(hours=hours_shift,minutes=minutes_shift)
    
    return open_time


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # if the control_dist is less then an entry in the dict then the required time is the dist times the rate in dict. 
    list_speed = [(0, 0), (200, 15), (400, 15), (600, 15), (1000, 11.428), (1300, 13.333)]
    
    more_time = 0
    for i in range(1, len(list_speed)):
        if(control_dist_km > list_speed[i][0]):
            # km / km/hr = hr --- hr * 60 = min
            more_time += (60 * (list_speed[i][0] - list_speed[i-1][0])) / list_speed[i][1]
        elif(control_dist_km <= list_speed[i][0]):
            if i == 0:
                pass
            else: #i != 0
                distance_remaining = control_dist_km - list_speed[i-1][0]
                more_time += (60 * distance_remaining) / list_speed[i][1]
                break

    more_time = round(more_time)
    hours_shift = more_time // 60
    minutes_shift = more_time % 60
    close_time = brevet_start_time.clone()
    close_time = close_time.shift(hours=hours_shift,minutes=minutes_shift)
    return close_time
